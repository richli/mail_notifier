use std::error::Error;
use vergen::EmitBuilder;

fn main() -> Result<(), Box<dyn Error>> {
    EmitBuilder::builder()
        .git_commit_timestamp()
        .git_sha(false)
        .fail_on_error()
        .emit()?;
    Ok(())
}
