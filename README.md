# Mail Notifier

This is a simple program to poll configured IMAP folders for new mail and then send a dbus notification. The IMAP IDLE command is used for "push notifications".

## Dependencies

This only runs on Linux. [Rust](https://www.rust-lang.org) is required, as well as headers for various system libraries. For Debian, this includes the following packages: `libdbus-1-dev` and `libssl-dev`. For Fedora, this includes the following packages: `dbus-devel` and `openssl-devel`.

## Building

This is built and distributed in Flatpak format.

First, the crates recorded in `Cargo.lock` need to be converted into a format for Flatpak using [this script](https://github.com/flatpak/flatpak-builder-tools/blob/master/cargo/flatpak-cargo-generator.py):

```
curl -O https://raw.githubusercontent.com/flatpak/flatpak-builder-tools/master/cargo/flatpak-cargo-generator.py
# Note that the Python toml and siphash packages are required
python ./flatpak-cargo-generator.py Cargo.lock -o data/generated-sources.json
```

Then to build everything into the `build-dir` directory and to update the ostree repo at `repo-dir`:

```
flatpak-builder --repo=repo-dir build-dir data/com.dranek.MailNotifier.json
```

This can be exported as a single-file bundle, `mail_notifier.flatpak`:

```
flatpak build-bundle --runtime-repo=https://flathub.org/repo/flathub.flatpakrepo repo-dir mail_notifier.flatpak com.dranek.MailNotifier
```

To install it locally in the per-user flatpak installation:

```
flatpak install --user --bundle mail_notifier.flatpak
```

To run it:

```
flatpak run com.dranek.MailNotifier
```

## Configuration file format

The configuration file is in [TOML](https://github.com/toml-lang/toml) format and looks like this:

```toml
[[mailboxes]]
domain = "imap.example.com"
port = 993
user = "user"
pass = "secret"
folder = "INBOX"

```

By default, the configuration is stored at
`${XDG_CONFIG_HOME}/mail_notifier/mail_config.toml`, which when run using
Flatpak, expands out by default to
`${HOME}/.var/app/com.dranek.MailNotifier/config/mail_notifier/mail_config.toml`.

## Interaction

The primary purpose of this program is to show notifications, but not all errors
will get show up as notifications. By default, when running, it only outputs to
stdout/stderr log messages that are at WARNING or ERROR levels. However all
DEBUG messages and higher are logged here:
`$XDG_CACHE_DIR/mail_notifier/$TIMESTAMP.log` where `$TIMESTAMP` is the time
that the program started.

Only one running instance of the application is allowed. This is maintained by
asking dbus for ownership of the well-known name `com.dranek.MailNotifier`.

## License

This work is licensed under the [MIT License](LICENSE).
