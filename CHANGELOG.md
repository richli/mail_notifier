# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.5.0] - 2020-04-27

### Added

- The commit hash and date are embedded during build time and displayed using the `--version` command
- Added a `.desktop` file
- Added a basic icon

### Changed

- Package using [Flatpak](https://flatpak.org/)
- Change channel backend from `std::sync::mpsc` to `crossbeam_channel`
- Logs are now written to disk and stored under `$XDG_CACHE_DIR/mail_notifier/`
- Notifications are sent directly using the `dbus` crate
- Only one instance is allowed to run at a time

### Removed

- Remove systemd/journald support
- Remove Arch `PKGBUILD`
- Remove `Makefile`
- Remove options to not use IMAP `IDLE` and to not send notifications

### Fixed

- Try to convert message date to local time
- "Un-notify" messages: close sent notifications if a message transitions from unseen to seen
- Decode [MIME encoded-word](https://en.wikipedia.org/wiki/MIME#Encoded-Word) message subjects

## [0.4.0] - 2019-01-05

### Added

- Notify systemd when service is started
- Specify target for log messages: stdout, stderr, or systemd-journald

### Changed

- Included systemd user service file now is a `Type=notify` service and the output is sent to the journal

## [0.3.0] - 2018-11-24

### Fixed

- Update `imap` crate to `0.9`, enabling OpenSSL 1.1.1 support

## [0.2.0] - 2018-03-03

### Added

- Include a systemd user service
- Add a `Makefile` to simplify installation
- Add a `PKGBUILD` for Arch Linux

### Changed

- Use `$XDG_CONFIG_HOME` for default configuration file directory instead of current working directory

### Fixed

- Terminate the program if any child thread sends an error
- Handle empty subject lines

## 0.1.0 - 2018-01-05

### Added

- Basic functionality completed
- IDLE is used for "push notifications"
- Dbus is used to send out desktop notifications
- Each IMAP mailbox is checked by a background thread

[Unreleased]: https://gitlab.com/richli/mail_notifier/compare/v0.5.0...master
[0.5.0]: https://gitlab.com/richli/mail_notifier/compare/v0.4.0...v0.5.0
[0.4.0]: https://gitlab.com/richli/mail_notifier/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/richli/mail_notifier/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/richli/mail_notifier/compare/v0.1.0...v0.2.0
