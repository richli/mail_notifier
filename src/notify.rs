//! Manages sending and closing desktop notifications
//!
//! This holds a set of notifications that have been sent in order to manage
//! closing them. It's possible to add a new notification, close a given
//! notification, close all except a given notification, and close all
//! notifications.
//!
//! Each notification is sent using `dbus`, but the exact method differs for
//! using the [conventional
//! notifications](https://developer.gnome.org/notification-spec/) using the
//! `org.freedesktop.Notifications` service or for using [portal
//! api](https://flatpak.github.io/xdg-desktop-portal/portal-docs.html#gdbus-org.freedesktop.portal.Notification)
//! using the `org.freedesktop.portal.Notification` service.
//!
//! If running within a flatpak, the `org.freedesktop.portal.Notification`
//! service should be used, otherwise the `org.freedesktop.Notifications` should
//! be used.

use crate::message::UnseenMessage;
use dbus::{
    arg::{RefArg, Variant},
    blocking::LocalConnection,
};
use imap::types::Uid;
use log::debug;
use std::{
    collections::{HashMap, HashSet},
    time::Duration,
};

const APP_NAME: &str = env!("CARGO_PKG_NAME");

/// This is a unique ID for an given message since it's tied to the mailbox
/// (represented as a string containing the server, username, and folder), the
/// `UID_VALIDITY` for the folder, and the `UID` for the actual message.
#[derive(Debug, Hash, PartialEq, Eq)]
pub struct MailId {
    pub folder: String,
    pub uid_validity: Uid,
    pub uid: Uid,
}

#[derive(Debug)]
pub enum NotifyType {
    Standard,
    Portal,
}

impl NotifyType {
    fn interface(&self) -> &str {
        match self {
            NotifyType::Portal => "org.freedesktop.portal.Notification",
            NotifyType::Standard => "org.freedesktop.Notifications",
        }
    }

    fn object_path(&self) -> &str {
        match self {
            NotifyType::Portal => "/org/freedesktop/portal/desktop",
            NotifyType::Standard => "/org/freedesktop/Notifications",
        }
    }

    fn bus_name(&self) -> &str {
        match self {
            NotifyType::Portal => "org.freedesktop.portal.Desktop",
            NotifyType::Standard => "org.freedesktop.Notifications",
        }
    }
}

#[derive(Debug)]
pub enum NotificationMap {
    Standard(HashMap<MailId, u32>),
    Portal(HashMap<MailId, String>),
}

/// Keep track of the messages that have been previously notified as well as the
/// notification ID associated with each.
pub struct NotifiedMessages<'a> {
    /// The opened dbus connection
    conn: &'a LocalConnection,
    /// The keys are unique for each message (combination of the message UID and
    /// the folder); the values are the notification IDs
    notifications: NotificationMap,
}

impl<'a> NotifiedMessages<'a> {
    /// Initialize a new manager
    pub fn new(conn: &'a LocalConnection, kind: NotifyType) -> Self {
        match kind {
            NotifyType::Portal => Self {
                conn,
                notifications: NotificationMap::Portal(HashMap::new()),
            },
            NotifyType::Standard => Self {
                conn,
                notifications: NotificationMap::Standard(HashMap::new()),
            },
        }
    }

    /// Send a notification for an unread message.
    ///
    /// Note that the internal state is checked to avoid duplicate
    /// notifications. The return value indicates whether the notification was
    /// actually sent or not.
    pub fn notify(&mut self, id: MailId, msg: UnseenMessage) -> Result<bool, dbus::Error> {
        match &mut self.notifications {
            NotificationMap::Portal(m) => {
                if m.contains_key(&id) {
                    debug!("Suppressing duplicate notification for {:?}: {}", id, msg);
                    return Ok(false);
                }

                let summary = msg.summary();

                let notification_id = format!("mail-{}-{}-{}", id.folder, id.uid_validity, id.uid);
                let mut notify_vardict: HashMap<&str, Variant<_>> = HashMap::new();
                notify_vardict.insert("title", Variant(Box::new(summary)));
                notify_vardict.insert("body", Variant(Box::new(format!("{}", msg))));
                notify_vardict.insert("priority", Variant(Box::new("normal".to_owned())));

                let proxy = self.conn.with_proxy(
                    NotifyType::Portal.bus_name(),
                    NotifyType::Portal.object_path(),
                    Duration::from_secs(5),
                );

                proxy.method_call(
                    NotifyType::Portal.interface(),
                    "AddNotification",
                    (&notification_id, notify_vardict),
                )?;
                debug!("Sent mail notification with id: {}", notification_id);
                m.insert(id, notification_id);

                Ok(true)
            }

            NotificationMap::Standard(m) => {
                if m.contains_key(&id) {
                    debug!("Suppressing duplicate notification for {:?}: {}", id, msg);
                    return Ok(false);
                }
                let summary = msg.summary();
                let actions: Vec<&str> = Vec::new();
                let mut hints: HashMap<&str, Variant<Box<dyn RefArg>>> = HashMap::new();
                let replaces_id: u32 = 0;
                let expire: i32 = 0;

                // Urgency levels are defined as 0 => low, 1 => normal, 2 => critical;
                // also set it as a resident notification and use the "email arrived"
                // category
                hints.insert("urgency", Variant(Box::new(1_u8)));
                hints.insert("resident", Variant(Box::new(true)));
                hints.insert("category", Variant("email.arrived".to_string().box_clone()));

                let proxy = self.conn.with_proxy(
                    NotifyType::Standard.bus_name(),
                    NotifyType::Standard.object_path(),
                    Duration::from_secs(5),
                );

                let (notification_id,): (u32,) = proxy.method_call(
                    NotifyType::Standard.interface(),
                    "Notify",
                    (
                        APP_NAME,
                        replaces_id,
                        "mail-unread",
                        summary,
                        format!("{}", msg),
                        actions,
                        hints,
                        expire,
                    ),
                )?;
                debug!("Sent mail notification, received id: {}", notification_id);
                m.insert(id, notification_id);

                Ok(true)
            }
        }
    }

    /// Close all notifications for the given `mailbox`/`uid_validity` except
    /// those that are in the input set of UIDs
    pub fn close_all_except(&mut self, mailbox: String, uid_validity: Uid, skip_ids: HashSet<Uid>) {
        match &mut self.notifications {
            NotificationMap::Portal(m) => {
                let proxy = self.conn.with_proxy(
                    NotifyType::Portal.bus_name(),
                    NotifyType::Portal.object_path(),
                    Duration::from_secs(5),
                );

                m.retain(|msg_uid, notification_id| {
                    if msg_uid.folder != mailbox
                        || msg_uid.uid_validity != uid_validity
                        || skip_ids.contains(&msg_uid.uid)
                    {
                        true
                    } else {
                        debug!(
                            "Closing notification for previously unseen message: {:?}",
                            msg_uid
                        );
                        debug!("Closing notification id: {}", notification_id);
                        let _: () = proxy
                            .method_call(
                                NotifyType::Portal.interface(),
                                "RemoveNotification",
                                (notification_id.as_str(),),
                            )
                            .expect("Couldn't close notification");
                        false
                    }
                });
            }
            NotificationMap::Standard(m) => {
                let proxy = self.conn.with_proxy(
                    NotifyType::Standard.bus_name(),
                    NotifyType::Standard.object_path(),
                    Duration::from_secs(5),
                );

                m.retain(|msg_uid, notification_id| {
                    if msg_uid.folder != mailbox
                        || msg_uid.uid_validity != uid_validity
                        || skip_ids.contains(&msg_uid.uid)
                    {
                        true
                    } else {
                        debug!(
                            "Closing notification for previously unseen message: {:?}",
                            msg_uid
                        );
                        debug!("Closing notification id: {}", notification_id);
                        let _: () = proxy
                            .method_call(
                                NotifyType::Standard.interface(),
                                "CloseNotification",
                                (*notification_id,),
                            )
                            .expect("Couldn't close notification");
                        false
                    }
                });
            }
        }
    }

    /// Just fire off a notification without caring about tracking it for later.
    /// This is for an error-type message.
    pub fn notify_once_error(&self, summary: &str, body: &str) -> Result<(), dbus::Error> {
        match self.notifications {
            NotificationMap::Portal(_) => {
                let id = format!("error-{}", summary);
                let mut notify_vardict: HashMap<&str, Variant<_>> = HashMap::new();
                notify_vardict.insert("title", Variant(Box::new(summary)));
                notify_vardict.insert("body", Variant(Box::new(body)));
                // notify_vardict.insert("icon", Variant(("themed", Variant("dialog-error"))));
                notify_vardict.insert("priority", Variant(Box::new("high")));

                let proxy = self.conn.with_proxy(
                    NotifyType::Portal.bus_name(),
                    NotifyType::Portal.object_path(),
                    Duration::from_secs(5),
                );

                proxy.method_call(
                    NotifyType::Portal.interface(),
                    "AddNotification",
                    (id, notify_vardict),
                )?;
                debug!("Sent error notification");
            }

            NotificationMap::Standard(_) => {
                let actions: Vec<&str> = Vec::new();
                let mut hints: HashMap<&str, Variant<_>> = HashMap::new();
                let replaces_id: u32 = 0;
                let expire: i32 = 0;

                // Urgency levels are defined as 0 => low, 1 => normal, 2 => critical
                hints.insert("urgency", Variant(2_u8));

                let proxy = self.conn.with_proxy(
                    NotifyType::Standard.bus_name(),
                    NotifyType::Standard.object_path(),
                    Duration::from_secs(5),
                );
                let (id,): (u32,) = proxy.method_call(
                    NotifyType::Standard.interface(),
                    "Notify",
                    (
                        APP_NAME,
                        replaces_id,
                        "dialog-error",
                        summary,
                        body,
                        actions,
                        hints,
                        expire,
                    ),
                )?;
                debug!("Sent error notification, received id: {}", id);
            }
        }

        Ok(())
    }

    /// Just fire off a notification without caring about tracking it for later.
    /// This is for an info-type message.
    pub fn notify_once_info(&self, summary: &str, body: &str) -> Result<(), dbus::Error> {
        match self.notifications {
            NotificationMap::Portal(_) => {
                let id = format!("info-{}", summary);
                let mut notify_vardict: HashMap<&str, Variant<_>> = HashMap::new();
                notify_vardict.insert("title", Variant(Box::new(summary)));
                notify_vardict.insert("body", Variant(Box::new(body)));
                // notify_vardict.insert("icon", Variant(("themed", Variant("dialog-information"))));
                notify_vardict.insert("priority", Variant(Box::new("low")));

                let proxy = self.conn.with_proxy(
                    NotifyType::Portal.bus_name(),
                    NotifyType::Portal.object_path(),
                    Duration::from_secs(5),
                );

                proxy.method_call(
                    NotifyType::Portal.interface(),
                    "AddNotification",
                    (id, notify_vardict),
                )?;
                debug!("Sent info notification");
            }

            NotificationMap::Standard(_) => {
                let actions: Vec<&str> = Vec::new();
                let mut hints: HashMap<&str, Variant<Box<dyn RefArg>>> = HashMap::new();
                let replaces_id: u32 = 0;
                let expire: i32 = 0;

                // Urgency levels are defined as 0 => low, 1 => normal, 2 => critical; also
                // set this as a transient notification in the network category
                hints.insert("urgency", Variant(Box::new(0_u8)));
                hints.insert("transient", Variant(Box::new(true)));
                hints.insert("category", Variant("network".to_string().box_clone()));

                let proxy = self.conn.with_proxy(
                    NotifyType::Standard.bus_name(),
                    NotifyType::Standard.object_path(),
                    Duration::from_secs(5),
                );
                let (id,): (u32,) = proxy.method_call(
                    NotifyType::Standard.interface(),
                    "Notify",
                    (
                        APP_NAME,
                        replaces_id,
                        "dialog-information",
                        summary,
                        body,
                        actions,
                        hints,
                        expire,
                    ),
                )?;
                debug!("Sent info notification, received id: {}", id);
            }
        }

        Ok(())
    }
}

impl Drop for NotifiedMessages<'_> {
    fn drop(&mut self) {
        // Close all notifications
        match &mut self.notifications {
            NotificationMap::Portal(m) => {
                let proxy = self.conn.with_proxy(
                    NotifyType::Portal.bus_name(),
                    NotifyType::Portal.object_path(),
                    Duration::from_secs(5),
                );
                m.drain().for_each(|(mail_id, notification_id)| {
                    debug!(
                        "Closing notification for previously unseen message: {:?}",
                        mail_id
                    );
                    debug!("Closing notification id: {}", notification_id);
                    let _: () = proxy
                        .method_call(
                            NotifyType::Portal.interface(),
                            "RemoveNotification",
                            (notification_id,),
                        )
                        .expect("Couldn't close notification");
                });
            }
            NotificationMap::Standard(m) => {
                let proxy = self.conn.with_proxy(
                    NotifyType::Standard.bus_name(),
                    NotifyType::Standard.object_path(),
                    Duration::from_secs(5),
                );
                m.drain().for_each(|(mail_id, notification_id)| {
                    debug!(
                        "Closing notification for previously unseen message: {:?}",
                        mail_id
                    );
                    debug!("Closing notification id: {}", notification_id);
                    let _: () = proxy
                        .method_call(
                            NotifyType::Standard.interface(),
                            "CloseNotification",
                            (notification_id,),
                        )
                        .expect("Couldn't close notification");
                });
            }
        }
    }
}
