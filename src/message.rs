//! An email message that is unseen and potentially will be notified for

use base64::prelude::{Engine as _, BASE64_STANDARD};
use charset::Charset;
use chrono::{DateTime, Local};
use imap_proto::Envelope;
use log::{debug, warn};
use std::fmt;

/// An unseen message to notify about
#[derive(Debug)]
pub struct UnseenMessage {
    from: Option<String>,
    subject: Option<String>,
    date: Option<String>,
}

impl fmt::Display for UnseenMessage {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let empty = String::from("");
        let no_subject = String::from("(no subject)");
        write!(
            f,
            "Subject: {}\n\
             From: {}\n\
             Date: {}",
            self.subject.as_ref().unwrap_or(&no_subject),
            self.from.as_ref().unwrap_or(&empty),
            self.date.as_ref().unwrap_or(&empty),
        )
    }
}

impl UnseenMessage {
    /// Create a new unseen message from an IMAP `Envelope`
    pub fn new(envelope: &Envelope) -> Self {
        let date = envelope.date.map(|d| {
            // Try to parse the date, convert to localtime, and reformat it into
            // a string representation. If the parsing fails, just use the
            // original string without modification. This is a best-effort time
            // conversion.
            //
            // The date should be in RFC 2822 format, but sometimes the timezone
            // name is appended (e.g., "(UTC)" or "(EST)"). `chrono` doesn't like
            // that, so trim it off if it exists.

            let d_str = String::from_utf8_lossy(d);
            let d_trim = trim_tz(&d_str);
            let local_tz = Local::now().timezone();
            match DateTime::parse_from_rfc2822(d_trim) {
                Ok(d) => {
                    let date_local = d.with_timezone(&local_tz);
                    date_local.to_rfc2822()
                }
                Err(e) => {
                    warn!("Cannot parse date header: {:?}; {}", e, d_str);
                    d_str.into_owned()
                }
            }
        });
        let subject = envelope
            .subject
            .map(|s| decode_encoded_word(String::from_utf8_lossy(s).as_ref()));
        let from_all = envelope.from.as_ref().unwrap();
        if from_all.len() > 1 {
            warn!("Multiple 'from' addresses?! {:?}", from_all);
        }
        let from = from_all.first().map(|f| {
            format!(
                "{} <{}@{}>",
                f.name
                    .map(|s| decode_encoded_word(String::from_utf8_lossy(s).as_ref()))
                    .unwrap_or_default(),
                f.mailbox.map(String::from_utf8_lossy).unwrap_or_default(),
                f.host.map(String::from_utf8_lossy).unwrap_or_default()
            )
        });

        UnseenMessage {
            subject,
            date,
            from,
        }
    }

    /// Create a "summary", appropriate for a notification title
    pub fn summary(&self) -> String {
        match self.from.as_ref() {
            Some(f) => format!("Unread mail from {}", f),
            None => String::from("Unread mail"),
        }
    }
}

/// Trim the excess timezone from the Date string, if it exists.
///
/// RFC 2822 (and RFC 5322, which updates it) specifies the format for a `Date`
/// field. However, some emails have an extra timezone appended. For example:
/// "Mon, 4 Mar 2019 20:04:08 +0000 (UTC)".
///
/// This returns the substring such that the timezone name is trimmed off.
fn trim_tz(input: &str) -> &str {
    match input.rfind('(') {
        Some(i) => &input[..i - 1],
        None => input,
    }
}

/// Decode the [MIME encoded-word][] value using [RFC 2047][rfc2047].
///
/// Since a Rust `String` is returned, it's converted to UTF-8. This is a
/// best-effort conversion. If it fails, the original input is returned.
///
/// [MIME encoded-word]: https://en.wikipedia.org/wiki/MIME#Encoded-Word
///
/// [rfc2047]: https://tools.ietf.org/html/rfc2047
fn decode_encoded_word(value: &str) -> String {
    let words: Vec<(bool, String)> = value
        .split_whitespace()
        .map(|word| {
            if word.starts_with("=?") && word.ends_with("?=") && word.matches('?').count() == 4 {
                debug!("Attempting to decode an encoded-word: '{}'", word);
                let mut parts = word.split('?');
                assert_eq!(parts.next().unwrap(), "=");
                let charset = parts.next().unwrap();
                let encoding = parts.next().unwrap();
                let encoded_text = parts.next().unwrap();
                assert_eq!(parts.next().unwrap(), "=");

                let enc = Encoding::parse(encoding);
                let cset = Charset::for_label(charset.as_bytes());

                // If the encoding or charset are unrecognized, just return the word unmodified
                if enc == Encoding::Unknown {
                    warn!("Unrecognized encoding: {}", encoding);
                    return (false, String::from(word));
                }
                let cset = match cset {
                    Some(c) => {
                        debug!("Character set: {}", c.name());
                        c
                    }
                    None => {
                        warn!("Unrecognized character set: {}", charset);
                        return (false, String::from(word));
                    }
                };

                let decoded_bytes = match enc {
                    // "Q encoding", which is similar to quoted-printable
                    Encoding::Q => q_decode(encoded_text),
                    Encoding::Base64 => BASE64_STANDARD.decode(encoded_text).map_err(|e| e.into()),
                    Encoding::Unknown => unreachable!(),
                };

                let decoded_bytes = decoded_bytes.unwrap_or_else(|e| {
                    warn!("Cannot decode the data: {}", e);
                    // Treat the bytestring as the original word
                    word.as_bytes().to_vec()
                });

                let decoded_string = cset.decode(&decoded_bytes).0.to_string();
                debug!("Encoded word ({}) decoded to: '{}'", word, decoded_string);
                (true, decoded_string)
            } else {
                (false, String::from(word))
            }
        })
        .collect();

    // The words are joined with whitespace, unless two adjacent encoded-words
    // are present, then there's no whitespace.
    if !words.is_empty() {
        let mut words = words.into_iter();
        // (This unwrap is safe because we already checked that that there's at
        // least one entry in the iterator.)
        let (mut prev_word_was_decoded, mut output) = words.next().unwrap();
        for (word_was_decoded, word) in words {
            if !(word_was_decoded && prev_word_was_decoded) {
                output.push(' ');
            }
            output.push_str(&word);
            prev_word_was_decoded = word_was_decoded;
        }
        output
    } else {
        String::new()
    }
}

/// The encoding in an encoded-word
#[derive(Debug, PartialEq)]
enum Encoding {
    Base64,
    Q,
    Unknown,
}

impl Encoding {
    fn parse(value: &str) -> Self {
        if value.eq_ignore_ascii_case("B") {
            Encoding::Base64
        } else if value.eq_ignore_ascii_case("Q") {
            Encoding::Q
        } else {
            Encoding::Unknown
        }
    }
}

/// Decode the "Q-encoded" string to a byte vector.
///
/// See [RFC 2047](https://tools.ietf.org/html/rfc2047) section 4.2.
fn q_decode(value: &str) -> Result<Vec<u8>, Box<dyn std::error::Error>> {
    // Convert to bytes and replace underscores with spaces
    let mut output: Vec<_> = value
        .bytes()
        .map(|byte| if byte == b'_' { b' ' } else { byte })
        .collect();

    // Replace the '=xx' values. If we go from right to left, then the indices
    // don't change as we shorten the output vector.
    for (index, _) in value.rmatch_indices('=') {
        let hex_string = &value[index + 1..index + 3];
        let byte_value = u8::from_str_radix(hex_string, 16)?;

        // Replace three bytes with one byte. The ".last()" is so the splice is
        // consumed.
        output
            .splice(index..index + 3, std::iter::once(byte_value))
            .last();
    }
    Ok(output)
}

#[cfg(test)]
mod tests {

    mod encoded_word {
        use super::super::decode_encoded_word;

        /// Run some test cases from RFC 2047
        #[test]
        fn rfc2047_cases() {
            let test_pairs = [
            ("=?ISO-8859-1?Q?a?=", "a"),
            ("=?US-ASCII?Q?Keith_Moore?=", "Keith Moore"),
            (
                "=?ISO-8859-1?Q?Keld_J=F8rn_Simonsen?=",
                "Keld Jørn Simonsen",
            ),
            ("=?ISO-8859-1?Q?Andr=E9?= Pirard", "André Pirard"),
            ("=?ISO-8859-1?B?SWYgeW91IGNhbiByZWFkIHRoaXMgeW8=?= =?ISO-8859-2?B?dSB1bmRlcnN0YW5kIHRoZSBleGFtcGxlLg==?=", "If you can read this you understand the example."),
            ("=?ISO-8859-1?Q?Olle_J=E4rnefors?= <ojarnef@admin.kth.se>", "Olle Järnefors <ojarnef@admin.kth.se>"),
        ];

            for (test_in, ref exp_out) in test_pairs.iter() {
                assert_eq!(decode_encoded_word(*test_in), *exp_out);
            }
        }

        /// Test a UTF-8 case
        #[test]
        fn utf8_case() {
            assert_eq!(
                decode_encoded_word("=?UTF-8?B?UE9EQUFDIFdlZWtseSBEaWdlc3Q=?="),
                "PODAAC Weekly Digest"
            );
        }

        /// These don't follow the spec, so should remain unaltered
        #[test]
        fn invalid_inputs() {
            let test_pairs = [
                // Doesn't end right
                ("=?ISO-8859-1?Q?a?", "=?ISO-8859-1?Q?a?"),
                // Doesn't start right
                ("?ISO-8859-1?Q?a?=", "?ISO-8859-1?Q?a?="),
                // Not a valid encoding (T)
                ("=?ISO-8859-1?T?a?=", "=?ISO-8859-1?T?a?="),
                // Not a valid character set
                ("=?FOO-8859-1?Q?a?=", "=?FOO-8859-1?Q?a?="),
            ];

            for (test_in, ref exp_out) in test_pairs.iter() {
                assert_eq!(decode_encoded_word(*test_in), *exp_out);
            }
        }
    }

    mod trim_tz {
        use super::super::trim_tz;

        #[test]
        fn valid_inputs() {
            assert_eq!(
                trim_tz("Mon, 4 Mar 2019 20:04:08 +0000 (UTC)"),
                "Mon, 4 Mar 2019 20:04:08 +0000"
            );

            assert_eq!(
                trim_tz("Mon, 4 Mar 2019 20:04:08 +0000"),
                "Mon, 4 Mar 2019 20:04:08 +0000"
            );
        }
    }
}
