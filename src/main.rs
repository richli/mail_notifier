use clap::{arg, command, ArgAction};
use crossbeam_utils::thread::scope;
use dbus::blocking::LocalConnection;
use imap::types::Uid;
use log::{debug, error, info, warn};
use serde_derive::Deserialize;
use std::{
    collections::HashSet,
    fs,
    path::{Path, PathBuf},
};

pub mod mailbox;
pub mod message;
pub mod notify;
use self::mailbox::{Mailbox, MailboxError};
use self::message::UnseenMessage;

const APP_BUS_NAME: &str = "com.dranek.MailNotifier";

#[derive(Deserialize)]
struct Config {
    mailboxes: Vec<Mailbox>,
}

/// The program options
#[derive(Debug)]
struct ProgramOptions {
    /// Configuration TOML file
    config: PathBuf,
    /// The verbosity level (0 and up)
    verbosity: u64,
}

impl ProgramOptions {
    /// Construct the program options by parsing the program arguments
    fn parse_args(long_version: &str) -> Self {
        let xdg_config_home = dirs_next::config_dir().expect("Cannot determine config directory");
        let config_dir = xdg_config_home.join(env!("CARGO_PKG_NAME"));
        let config_default = config_dir.join("mail_config.toml");

        let matches = command!()
            .long_version(long_version.to_owned())
            .about("Notify on new mail")
            .args(&[
                arg!(verbose: -v --verbose ... "Sets the level of verbosity")
                    .action(ArgAction::Count),
                arg!(config: -c --config <CONFIG> "Configuration file to use")
                    .required(false)
                    .default_value(config_default.into_os_string())
                    .value_parser(clap::value_parser!(PathBuf)),
            ])
            .get_matches();

        ProgramOptions {
            config: matches.get_one::<PathBuf>("config").cloned().unwrap(),
            verbosity: matches.get_count("verbose").into(),
        }
    }
}

/// Initialize logging.
///
/// Logging to stdout/stderr is set by the command-line verbosity option.
/// Additionally, logs at debug level and higher are written to a log file in
/// the XDG_CACHE_DIR directory.
///
/// Note that the term logger may fail if run without a tty or otherwise
/// detached. This is allowed.
///
/// *Panics* if logging didn't succeed for some other reason.
fn initialize_logging(opts: &ProgramOptions) {
    let log_dir = dirs_next::cache_dir()
        .expect("Cannot determine cache directory")
        .join(env!("CARGO_PKG_NAME"));
    fs::create_dir_all(&log_dir).expect("Couldn't create log directory");

    let timestamp = chrono::Local::now();
    let log_file = log_dir.join(format!("{}.log", timestamp.format("%Y-%m-%dT%H:%M:%S%z")));

    let term_log_level = match opts.verbosity {
        0 => log::LevelFilter::Warn,
        1 => log::LevelFilter::Info,
        _ => log::LevelFilter::Debug,
    };
    let mut log_config = simplelog::ConfigBuilder::new();
    log_config
        .add_filter_allow(env!("CARGO_PKG_NAME").to_string())
        .set_time_offset_to_local()
        .expect("Couldn't set time offset")
        .set_time_format_rfc3339();

    let file_log = simplelog::WriteLogger::new(
        log::LevelFilter::Debug,
        log_config.build(),
        fs::File::create(log_file).unwrap(),
    );
    let term_log = simplelog::TermLogger::new(
        term_log_level,
        log_config.build(),
        simplelog::TerminalMode::Mixed,
        simplelog::ColorChoice::Auto,
    );
    simplelog::CombinedLogger::init(vec![term_log, file_log]).expect("Couldn't initialize logging");
}

// Try to acquire ownership of a well-known name and exit if some other
// process owns it
fn ensure_singleton(
    conn: &dbus::blocking::LocalConnection,
    bus_name: &str,
) -> Result<(), dbus::Error> {
    use dbus::blocking::stdintf::org_freedesktop_dbus::RequestNameReply;
    match conn.request_name(bus_name, false, false, true)? {
        RequestNameReply::PrimaryOwner => {
            debug!("Dbus has given us primary ownership of: {}", bus_name)
        }
        RequestNameReply::Exists => {
            error!("Cannot acquire ownership from dbus for: {}", bus_name);
            std::process::exit(1);
        }
        RequestNameReply::InQueue => {
            // We explicitly asked not to be queued, so this branch isn't
            // expected to happen
            error!(
                "Tried to claim ownership of {} but dbus put us in a queue?!",
                bus_name
            );
            std::process::exit(1);
        }
        RequestNameReply::AlreadyOwner => {
            // This seems unlikely to happen?
            error!(
                "Tried to claim ownership of {} but we already own it?!",
                bus_name
            );
            std::process::exit(1);
        }
    }
    Ok(())
}

/// See if we're running inside Flatpak or not
fn check_if_flatpak() -> bool {
    // There are probably a few ways to check for this, such as if the
    // FLATPAK_ID environment variable is set. Instead, check if a special file
    // exists:
    // https://blogs.gnome.org/mclasen/2018/07/02/flatpak-in-detail-part-3/
    Path::new("/.flatpak-info").exists()
}

/// Information sent from the child threads to the main threads about changes in
/// the notification state.
///
/// Note that the tuple (`mailbox`, `uid_validity`, `uid`) constitutes a unique
/// indentifier for a message.
#[derive(Debug)]
pub enum NotifyCommands {
    /// Create a new notification for the given message
    New {
        mailbox: String,
        uid_validity: Uid,
        uid: Uid,
        msg: UnseenMessage,
    },
    /// Close all notifications except these ones
    CloseAllExcept {
        mailbox: String,
        uid_validity: Uid,
        uids: HashSet<Uid>,
    },
}

fn main() {
    let long_version = match (
        option_env!("VERGEN_GIT_COMMIT_TIMESTAMP"),
        option_env!("VERGEN_GIT_SHA"),
    ) {
        (Some(date), Some(sha)) => format!("{} ({} {})", env!("CARGO_PKG_VERSION"), date, sha),
        _ => env!("CARGO_PKG_VERSION").to_string(),
    };

    let opts = ProgramOptions::parse_args(&long_version);
    initialize_logging(&opts);
    info!("{} {}", env!("CARGO_PKG_NAME"), &long_version);

    let dbus_conn = LocalConnection::new_session().expect("Cannot connect to session dbus");
    debug!(
        "Connected to session dbus with unique name: {}",
        dbus_conn.unique_name()
    );

    let mut notified_messages = if check_if_flatpak() {
        info!("Flatpak mode detected");
        notify::NotifiedMessages::new(&dbus_conn, notify::NotifyType::Portal)
    } else {
        info!("Flatpak mode not detected");
        notify::NotifiedMessages::new(&dbus_conn, notify::NotifyType::Standard)
    };

    // Try to acquire ownership of a well-known name and exit if some other
    // process owns it. Note that we need to keep this dbus connection open for
    // the lifetime of the process.
    ensure_singleton(&dbus_conn, APP_BUS_NAME).expect("Cannot request name from dbus");

    info!("Reading config file: {}", opts.config.display());
    let config: Config = if !opts.config.exists() {
        error!("Config file doesn't exist: {}", opts.config.display());
        notified_messages
            .notify_once_error(
                "No config for mail notifier",
                &format!("{}", opts.config.display()),
            )
            .unwrap();
        std::process::exit(1);
    } else {
        let conf_contents = fs::read_to_string(&opts.config).expect("Cannot read config file");
        toml::from_str(&conf_contents).expect("Cannot parse config file")
    };

    debug!(
        "Spawning {} threads to check the mailboxes",
        config.mailboxes.len()
    );
    let (tx, rx) = crossbeam_channel::bounded(5);
    notified_messages
        .notify_once_info(
            "Starting mail notifier",
            &format!("{} folders monitored", config.mailboxes.len()),
        )
        .unwrap();

    // Here, we spawn a thread for each mailbox to check. The crossbeam channel
    // is a MPSC channel, so each child thread is a producer of
    // `NotifyCommands`s and the main thread consumes them, using its dbus
    // connection to send and close notifications.
    scope(|s| {
        for mbox in config.mailboxes {
            info!("Checking mail for: {}", mbox);

            let tx = tx.clone();
            s.spawn(move |_| {
                // Each child thread is responsible for re-trying the querying
                // in case the connection gets disconnected. But if there's some
                // other error, then bring down the whole process.
                loop {
                    let tx = tx.clone();
                    if let Err(mail_err) = mbox.query_for_mail(tx) {
                        match mail_err {
                            MailboxError::ImapError {
                                folder,
                                source: imap::error::Error::ConnectionLost,
                            } => info!("Connection lost retrying ({})", folder),
                            MailboxError::ImapError {
                                folder,
                                source: imap::error::Error::Io(ioerr),
                            } => warn!("I/O error ({}), retrying ({})", ioerr, folder),
                            _ => {
                                error!("{:?}", mail_err);
                                std::process::exit(1)
                            }
                        }
                    }
                }
            });
        }

        // Consume the incoming messages. Notifications are issued for each
        // mailbox and cleaned up.
        drop(tx);
        for cmd in rx.iter() {
            match cmd {
                NotifyCommands::New {
                    mailbox,
                    uid_validity,
                    uid,
                    msg,
                } => {
                    let mail_id = notify::MailId {
                        folder: mailbox,
                        uid_validity,
                        uid,
                    };
                    notified_messages
                        .notify(mail_id, msg)
                        .expect("Couldn't notify");
                }
                NotifyCommands::CloseAllExcept {
                    mailbox,
                    uid_validity,
                    uids,
                } => notified_messages.close_all_except(mailbox, uid_validity, uids),
            }
        }
    })
    .unwrap();
}
