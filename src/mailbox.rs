//! An IMAP mailbox to monitor for unseen messages

use crate::message::UnseenMessage;
use crate::NotifyCommands;
use crossbeam_channel::Sender;
use imap;
use log::{debug, info};
use native_tls::TlsConnector;
use serde_derive::Deserialize;
use std::fmt;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum MailboxError {
    #[error("IO error: {0}")]
    IoError(#[from] std::io::Error),
    #[error("IMAP error on {folder}: {source}")]
    ImapError {
        folder: String,
        source: imap::error::Error,
    },
    #[error("Unsupported server on {folder}: {msg}")]
    Unsupported { folder: String, msg: String },
}

#[derive(Deserialize, Debug, Clone)]
pub struct Mailbox {
    pub domain: String,
    pub port: u16,
    pub user: String,
    pub pass: String,
    pub folder: String,
}

impl fmt::Display for Mailbox {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "imaps://{}@{}:{}/{}",
            self.user, self.domain, self.port, self.folder
        )
    }
}

impl Mailbox {
    /// Query the mailbox for new mail
    pub fn query_for_mail(&self, tx: Sender<NotifyCommands>) -> Result<(), MailboxError> {
        debug!("Connecting to IMAP server ({}:{})", self.domain, self.port);

        let socket_addr = (self.domain.as_str(), self.port);
        let tls_connector = TlsConnector::builder().build().unwrap();
        let client = imap::connect(socket_addr, self.domain.as_str(), &tls_connector)
            .expect("Cannot connect to IMAP server");

        debug!("Authenticating ({})", self.user);
        let mut imap_session = client
            .login(self.user.as_str(), self.pass.as_str())
            .expect("Cannot authenticate");

        // Continually loop to query the folder. Note that in the error-free
        // case, this is an infinite loop, so there's no need to log out of the
        // IMAP session. If an error happens, then we try to log out on the way
        // out of this function.
        loop {
            // Query some mailbox properties in a read-only manner. Also if this
            // is the first time, then the IMAP state moves from "authenticated"
            // to "selected".
            debug!("Querying folder ({})", self.folder);
            let folder = imap_session.examine(self.folder.as_str()).map_err(|e| {
                // Try to log out but ignore any error from doing so
                imap_session.logout().ok();
                MailboxError::ImapError {
                    source: e,
                    folder: self.folder.clone(),
                }
            })?;

            let uid_validity = folder.uid_validity.ok_or_else(|| {
                // If this is None, then the server doesn't support UIDs. Let's just exit out.
                imap_session.logout().ok();
                MailboxError::Unsupported {
                    folder: self.folder.clone(),
                    msg: "Server doesn't support UID".to_string(),
                }
            })?;

            // Find all unseen UIDs in the folder and query the corresponding
            // envelopes. These will all be sent over the channel as new
            // messages to notify for. (On the receiving end, it checks that
            // previously notified messages don't get re-notified.)
            let unseen_ids = imap_session.uid_search("UNSEEN").map_err(|e| {
                // Try to log out but ignore any error from doing so
                imap_session.logout().ok();

                MailboxError::ImapError {
                    source: e,
                    folder: self.folder.clone(),
                }
            })?;
            info!(
                "{}/{}: {} messages; {} unread messages",
                self.domain,
                self.folder,
                folder.exists,
                unseen_ids.len(),
            );

            // Convert each (newly) unseen message UID to an `UnseenMessage` and
            // send it over the channel
            unseen_ids.iter().for_each(|uid| {
                let uid_messages = imap_session
                    .uid_fetch(&uid.to_string(), "ENVELOPE")
                    .unwrap();
                assert_eq!(uid_messages.len(), 1);

                let unseen_msg = UnseenMessage::new(uid_messages[0].envelope().unwrap());
                info!("Unread message: {}", unseen_msg);

                tx.send(NotifyCommands::New {
                    mailbox: format!("{}", *self),
                    uid_validity,
                    uid: *uid,
                    msg: unseen_msg,
                })
                .expect("Message sending failed");
            });

            // Clean up messages that were previously notified for but have
            // since transitioned from unseen to seen
            tx.send(NotifyCommands::CloseAllExcept {
                mailbox: format!("{}", *self),
                uid_validity,
                uids: unseen_ids,
            })
            .expect("Message sending failed");

            // If we're using IDLE, then wait until we get woken up the server
            // (it may be a new mail, but it could be a different event).
            // Otherwise, we're finished and exit the loop. Note that in idle
            // mode, there is no exit to this loop unless the program is
            // terminated.
            let idle_handle = imap_session.idle().map_err(|e| MailboxError::ImapError {
                source: e,
                folder: self.folder.clone(),
            })?;
            debug!("IDLE on {}", self.folder);
            idle_handle.wait_keepalive().map_err(|e| {
                // Try to log out but ignore any error from doing so
                imap_session.logout().ok();
                MailboxError::ImapError {
                    source: e,
                    folder: self.folder.clone(),
                }
            })?;
            debug!("IDLE finished on {}", self.folder);
        }
    }
}
